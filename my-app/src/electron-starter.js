const { app, BrowserWindow } = require('electron');

const path = require('path');
const url = require('url');

let mainWindow;

function createWindow() {
    // Create the browser window and load index.html
    mainWindow = new BrowserWindow({width: 800, height: 600});
    mainWindow.loadURL('http://localhost:3001');

    // Open dev tools.
    mainWindow.webContents.openDevTools();

    // Fired when window is closed.
    mainWindow.on('closed', () => {
        mainWindow = null
    })
}

app.on('ready', createWindow);